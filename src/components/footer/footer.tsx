import style from './footer.module.css'

export default function Footer() {
  return (
		<footer className={style.footer}>
			<div className={style.footer_wrap}>
				<a className={style.footer_mail} href="mailto:brodich_vlad@ukr.net">
					brodich_vlad@ukr.net
				</a>
				<p className={style.footer_date} lang="uk">
					18 Січня 2024р.
				</p>
			</div>
		</footer>
	);
}