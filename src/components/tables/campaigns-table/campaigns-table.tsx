import style from './campaigns-table.module.css';
import { thousandSeparator } from '../../../lib/utils/thousand-separator';
import { Campaign } from '../../../types/account.type';
import { CampaignTableProps } from './CampaignTable.type';
import UseButton from '../../ui/use-button/use-button';

export default function CampaignTable(props: CampaignTableProps): JSX.Element {

	const { data, onClickClicksMin, onClickClicksMax, onClickCostMin, onClickCostsMax } = props;

	function createTableBody(dataAccount: Campaign[]) {
		return dataAccount.map(e => {
			return (
				<tr key={e.campaignId} className={style.table_row}>
					<td>{e.campaignId}</td>
					<td>{thousandSeparator(e.clicks)}</td>
					<td>{`${thousandSeparator(e.cost)} $`}</td>
					<td>{e.date}</td>
				</tr>
			);
		});
	}

	return (
		<table className={style.table}>
			<thead>
				<tr className={style.thead}>
					<th>Campaign ID</th>
					<th>
						<div className={style.thead_sort}>
							<span>Clicks</span>
							<div className={style.sort_btns}>
								<UseButton onClick={onClickClicksMin} className={style.sort_btn} children={'min'} />
								<UseButton onClick={onClickClicksMax} className={style.sort_btn} children={'max'} />
							</div>
						</div>
					</th>
					<th>
						<div className={style.thead_sort}>
							<span>Cost</span>
							<div className={style.sort_btns}>
								<UseButton
									onClick={onClickCostMin}
									className={style.sort_btn}
									children={'min'}
								/>
								<UseButton
									onClick={onClickCostsMax}
									className={style.sort_btn}
									children={'max'}
								/>
							</div>
						</div>	
					</th>
					<th>Date</th>
				</tr>
			</thead>
			<tbody>{createTableBody(data)}</tbody>
		</table>
	);
}
