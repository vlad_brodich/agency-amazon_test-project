import { Campaign } from "../../../types/account.type";

export interface CampaignTableProps {
	data: Campaign[];
	onClickClicksMin: () => void
	onClickClicksMax: () => void
	onClickCostMin: () => void
	onClickCostsMax: () => void
}