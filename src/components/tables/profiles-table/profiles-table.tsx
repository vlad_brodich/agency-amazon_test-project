import style from './profiles-table.module.css';
import { Profile } from '../../../types/account.type';
import { ProfilesTableProps } from './ProfilesTableProps.type';
import UseButton from '../../ui/use-button/use-button';

export default function ProfilesTable(props: ProfilesTableProps): JSX.Element {

	const { data, location, onClickCountryAz, onClickCountryZa, onClickMarketplaceAz, onClickMarketplaceZa } = props
	
	function createTable(dataAccount: Profile[]) {
		return dataAccount.map(e => {
			return (
				<tr
					key={e.profileId}
					onClick={() => {
						location(e.profileId);
					}}
					className={style.table_row}
				>
					<td>{e.profileId}</td>
					<td>{e.country}</td>
					<td>{e.marketplace}</td>
				</tr>
			);
		});
	}

	return (
		<table className={style.table}>
			<thead>
				<tr className={style.thead}>
					<th>Profile ID</th>
					<th>
						<div className={style.thead_sort}>
							<span>Country</span>
							<div className={style.sort_btns}>
								<UseButton onClick={onClickCountryAz} className={style.sort_btn} children={'A-z'} />
								<UseButton onClick={onClickCountryZa} className={style.sort_btn} children={'Z-a'} />
							</div>
						</div>
					</th>
					<th>
						<div className={style.thead_sort}>
							<span>Marketplace</span>
							<div className={style.sort_btns}>
								<UseButton onClick={onClickMarketplaceAz} className={style.sort_btn} children={'A-z'} />
								<UseButton onClick={onClickMarketplaceZa} className={style.sort_btn} children={'Z-a'} />
							</div>
						</div>
					</th>
				</tr>
			</thead>

			<tbody>{createTable(data)}</tbody>
		</table>
	);
}
