import { Profile } from "../../../types/account.type";

export interface ProfilesTableProps {
	data: Profile[];
	location: (id: string) => void;
	onClickCountryAz: () => void;
	onClickCountryZa: () => void;
	onClickMarketplaceAz: () => void;
	onClickMarketplaceZa: () => void;
}