import style from './accounts-table.module.css';
import { Account } from '../../../types/account.type';
import { AccountTableProps } from './AccountTableProps.type';
import UseButton from '../../ui/use-button/use-button';
import SearchBar from '../../ui/search-bar/search-dar';

export default function AccountTable(props: AccountTableProps): JSX.Element {
	const { data,inputValue, location, onSortAccountsEmailAz, onSortAccountsEmailZa, onChangeInput } = props;

	function createTable(dataAccounts: Account[]): JSX.Element[] {
		return dataAccounts.map(e => {
			return (
				<tr
					onClick={() => {
						location(e.accountId);
					}}
					key={e.accountId}
					className={style.table_row}
				>
					<td>{e.accountId} </td>
					<td>{e.email} </td>
					<td>{e.authToken} </td>
					<td>{e.creationDate} </td>
				</tr>
			);
		});
	}

	return (
		<table className={style.table}>
			<thead>
				<tr className={style.thead}>
					<th>Account ID</th>
					<th>
						<div className={style.thead_sort}>
							<span>Email</span>
							<SearchBar onChange={onChangeInput} inputValue={inputValue} />
							<div className={style.sort_btns}>
								<UseButton onClick={onSortAccountsEmailAz} className={style.sort_btn} children={'A-z'} />
								<UseButton onClick={onSortAccountsEmailZa} className={style.sort_btn} children={'Z-a'} />
							</div>
						</div>	
					</th>
					<th>Auth Token</th>
					<th>Creation date</th>
				</tr>
			</thead>
			<tbody>
				{data ? (
					createTable(data)
				) : (
					<tr className={style.error}>
						<td colSpan={4}>No data found.</td>
					</tr>
				)}
			</tbody>
		</table>
	);
}
