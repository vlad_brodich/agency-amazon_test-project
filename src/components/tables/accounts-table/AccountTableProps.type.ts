import { Account } from "../../../types/account.type";

export interface AccountTableProps {
	data: Account[]| null;
	inputValue:string
	location: (id: string) => void;
	onSortAccountsEmailAz: () => void;
	onSortAccountsEmailZa: () => void;
	onChangeInput: (arg: string) => void;
}