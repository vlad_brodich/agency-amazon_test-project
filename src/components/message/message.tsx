import style from './message.module.css';

interface PropTypes{
  content:string
}

export default function Message({ content }: PropTypes) {
	return (
		<div className={style.message}>
			<h2 className={style.message__content}>{content}</h2>
		</div>
	);
}