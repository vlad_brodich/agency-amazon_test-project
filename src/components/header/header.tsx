import { Link } from 'react-router-dom';
import style from './header.module.css'

export default function Header() {
  return (
		<header>
			<div className={style.header_wrap}>
				<Link className={style.logo} to={'/'}>
					<span className={style.logo_yllow}>V</span>
					<span className={style.logo_blue}>B</span>
				</Link>
			</div>
		</header>
	);
}