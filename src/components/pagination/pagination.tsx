import { PaginationProps } from './PaginationProps.type';
import Styles from './pagination.module.css';
import UseButton from '../ui/use-button/use-button';

export default function Pagination (props: PaginationProps){
  const { nav = null, disable, onNextPageClick, onPrevPageClick } = props;

  return (
		<div className={Styles.paginator}>
			<UseButton
				className={Styles.arrow}
				onClick={onPrevPageClick}
				disable={disable.left}
				children={<>&#10094; Prev</>}
			/>
			{nav && (
				<span className={Styles.navigation}>
					{nav.current} / {nav.total}
				</span>
			)}
			<UseButton
				className={Styles.arrow}
				onClick={onNextPageClick}
				disable={disable.right}
				children={<>Next &#10095;</>}
			/>
		</div>
	);
}

