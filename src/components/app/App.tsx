import {
  createBrowserRouter,
  createRoutesFromElements,
  Route,
  RouterProvider,
} from "react-router-dom";
import ErrorPage from '../../pages/error-page/error-page';
import Layout from '../../pages/layout-page/layout';
import ProfilesPage from '../../pages/profiles-page/profiles-page';
import CompaignsPage from '../../pages/campaigns-page/campaigns-page';
import AccountsPage from '../../pages/accounts-page/accounts-page';

export default function App() {
	const router = createBrowserRouter(
		createRoutesFromElements(
			<Route path="/" element={<Layout />}>
				<Route index element={<AccountsPage />}/>
				<Route path={`profiles/:id`} element={<ProfilesPage />}/>
				<Route path="profiles/:id/campaigns/:id" element={<CompaignsPage />}/>
				<Route path="*" element={<ErrorPage />}/>
			</Route>
		)
	);

	return <RouterProvider router={router} />
}

