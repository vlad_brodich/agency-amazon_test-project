import { UseButtonProps } from "./UseButtonProps.type";

export default function UseButton(props: UseButtonProps):JSX.Element{
	const { className, disable = false, children, onClick } = props;
	
	return (
		<button className={className} type="button" onClick={onClick} disabled={disable}>
			{children}
		</button>
	);
}
