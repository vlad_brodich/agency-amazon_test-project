export interface UseButtonProps {
	onClick: () => void;
	disable?: boolean;
	className: string;
	children: JSX.Element | string;
}
