export interface SearchBarProps {
  onChange: (arg: string) => void
  inputValue:string
}