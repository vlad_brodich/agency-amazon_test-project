import { SearchBarProps } from './SearchBarProps.type';
import style from './search-dar.module.css';
import { CiSearch } from 'react-icons/ci';

export default function SearchBar(props: SearchBarProps) {
	return (
		<div className={style.search}>
			<span className={style.search_icon}>
				<CiSearch />
			</span>
			<input
        className={style.search_input}
        value={props.inputValue}
				type="search"
				onChange={e => {
					props.onChange(e.target.value);
				}}
				placeholder={'Search...'}
			/>
		</div>
	);
}
