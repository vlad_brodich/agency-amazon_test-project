import { Outlet } from 'react-router-dom';
import style from './layout.module.css';
import Header from '../../components/header/header';
import Footer from '../../components/footer/footer';

export default function Layout(): JSX.Element {
	return (
		<>
			<Header />
			<main className={style.main}>{<Outlet />}</main>
			<Footer/>
		</>
	);
}
