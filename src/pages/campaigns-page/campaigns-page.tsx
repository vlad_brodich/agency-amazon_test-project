import style from './campaigns-page.module.css';
import { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { Campaign } from '../../types/account.type';

import { getProfileCompaigns } from '../../services/get-profile-compaigns';
import Loading from '../../components/loading/loading';
import CampaignTable from '../../components/tables/campaigns-table/campaigns-table';
import Message from '../../components/message/message';
import UseButton from '../../components/ui/use-button/use-button';

export default function CompaignsPage(): JSX.Element {
	// Отримання id сторінки.
	const { id } = useParams();
	const navigate = useNavigate();

	// Стан сторінки.
	const [campaigns, setCampaigns] = useState<Campaign[]|null>(null);
	const [sortData, setSortData] = useState<Campaign[]|null>(null);
	const [loading, setLoading] = useState<boolean>(true);
	const [error, setError] = useState<string | null>(null);
	
	// Отримання даних з сервера.
	useEffect(() => {
		setError(null);
		if (!id) {
			setError('Unknown Error: id')
			return
		};
		const fetchData = async () => {
			setError(null);
			setLoading(true);
			try {
				const response = await getProfileCompaigns(id);
				setCampaigns(response)
				setSortData(response)
			} catch (err) {
				setError(err instanceof Error ? err.message : 'Unknown Error: getProfileCompaigns');
				setCampaigns([])
				setSortData(null)
			} finally {
				setLoading(false);
			}
		};
		fetchData();
	}, []);

	// Сортування елементів.
	function sortCampaignsDataMaxMin(arg:'clicks'|'cost') {
		if (!campaigns) { return };
		const res = [...campaigns].sort(function (a, b) {
			return b[arg] - a[arg]
		});
		setSortData([...res])
	}

	function sortCampaignsDataMinMax(arg:'clicks'|'cost') {
		if (!campaigns) { return };
		const res = [...campaigns].sort(function (a, b) {
			return a[arg] - b[arg]
		});
		setSortData([...res])
	}

	const clicksMinMax = (): void => { sortCampaignsDataMinMax('clicks') }
	const clicksMaxMin = (): void => { sortCampaignsDataMaxMin('clicks') }

	const costMinMax = (): void => { sortCampaignsDataMinMax('cost') }
	const costMaxMin = (): void => { sortCampaignsDataMaxMin('cost') }


	if (loading) return <Loading />;
	if (error) return <Message content={error} />;
	return (
		<>
			<UseButton
				onClick={() => navigate(-1)}
				children={<>&#10094; Go back</>}
				className={style.go_back_btn}
			/>
			<h1>Compaigns</h1>
			{sortData ? (
				<CampaignTable
					onClickClicksMin={clicksMinMax}
					onClickClicksMax={clicksMaxMin}
					onClickCostMin={costMinMax}
					onClickCostsMax={costMaxMin}
					data={sortData}
				/>
			) : (
				<Message content={'Unfortunately, there are no data'} />
			)}
		</>
	)
}
