import style from './profiles-page.module.css';
import { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';

import { Profile } from '../../types/account.type';

import { getAccountProfiles } from '../../services/get-account-profiles';
import Loading from '../../components/loading/loading';
import Message from '../../components/message/message';
import ProfilesTable from '../../components/tables/profiles-table/profiles-table';
import UseButton from '../../components/ui/use-button/use-button';

export default function ProfilesPage(): JSX.Element {
	// Отримання id сторінки.
	const { id } = useParams();

	// Навігація між сторінками.
	const navigate = useNavigate();

	const location = (id: string): void => {
		navigate(`campaigns/${id}`);
	};
	
	const [profiles, setProfiles] = useState<Profile[]>([]);
	const [sortData, setSortData] = useState<Profile[]|null>(null);
	const [loading, setLoading] = useState<boolean>(true);
	const [error, setError] = useState<string | null>(null);

	// Отримання даних з сервера.
	useEffect(() => {
		setError(null);
		if (!id) {
			setError('Unknown Error: id')
			return
		};
		const fetchData = async () => {
			setError(null);
			setLoading(true);
			try {
				const response = await getAccountProfiles(id);
				setProfiles(response)
				setSortData(response)
			} catch (err) {
				setError(err instanceof Error ? err.message : 'Unknown Error: getProfileCompaigns');
				setProfiles([])
				setSortData(null)
			} finally {
				setLoading(false);
			}
		};
		fetchData();
	}, []);

		// Сортування елементів.
	function sortProfilesDataAz(arg:"country"|"marketplace") {
		if (!profiles) { return };
		const res = [...profiles].sort(function (a, b) {
			if (a[arg] > b[arg]) {
				return 1;
			}
			if (a[arg] < b[arg]) {
				return -1;
			}
			return 0;
		});
		setSortData([...res])
	}

	function sortProfilesDataZa(arg:"country"|"marketplace") {
		if (!profiles) { return };
		const res = [...profiles].sort(function (a, b) {
			if (a[arg] > b[arg]) {
				return -1;
			}
			if (a[arg] < b[arg]) {
				return 1;
			}
			return 0;
		});
		setSortData([...res])
	}

	const countryAz = (): void => { sortProfilesDataAz('country') }
	const countryZa = (): void => { sortProfilesDataZa('country') }

	const marketplaceAz = (): void => { sortProfilesDataAz('marketplace') }
	const marketplaceZa = (): void => { sortProfilesDataZa('marketplace') }

	if (loading) return <Loading />;
	if (error) return <Message content={error} />;

	return (
		<>
			<UseButton onClick={() => navigate(-1)} children={<>&#10094; Go back</>} className={style.go_back_btn} />
			<h1>Profiles</h1>
			{sortData ? (
				<ProfilesTable
					data={sortData}
					location={location}
					onClickCountryAz={countryAz}
					onClickCountryZa={countryZa}
					onClickMarketplaceAz={marketplaceAz}
					onClickMarketplaceZa={marketplaceZa}
				/>
			) : (
				<Message content={'Unfortunately, there are no data'} />
			)}
		</>
	);
}
