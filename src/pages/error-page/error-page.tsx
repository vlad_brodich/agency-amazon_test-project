import style from './error-page.module.css';
import { Link, useRouteError } from "react-router-dom";

export default function ErrorPage() {
  const error = useRouteError();
  console.error(error);

  return (
    <div className={style.error_page}>
      <div className={style.error_content}>
        <h1>Oops!</h1>
        <p>Unfortunately, such a page does not exist.</p>
        <p>{`Error: ${error}`}.</p>
        <Link to={'/'}>To the main page</Link>
      </div>
    </div> 
  );
}