import { useCallback, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Account } from '../../types/account.type';

import { createDataPages, getAccounts } from '../../services/get-accounts';
import AccountTable from '../../components/tables/accounts-table/accounts-table';
import Loading from '../../components/loading/loading';
import Message from '../../components/message/message';
import Pagination from '../../components/pagination/pagination';

export default function AccountsPage(): JSX.Element {
	// Навігація між сторінками.
	const navigate = useNavigate();
	const location = (id: string): void => {
		navigate(`profiles/${id}`);
	};
	const [accounts, setAccounts] = useState<Account[]>([]);
	const [searcAaccounts, setSearcAaccounts] = useState<Account[]|null>(null);
	const [pagesData, setPagesData] = useState<Account[][]>([]);
	const [page, setPage] = useState(1);
	const [inputValue, setInputValue] = useState<string>('');

	const [loading, setLoading] = useState<boolean>(true);
	const [error, setError] = useState<string | null>(null);

	// Отримання даних з сервера.
	useEffect(() => {
		const fetchData = async () => {
			setLoading(true);
			setError(null);
			try {
				const response = await getAccounts();
				setAccounts(response);
				setSearcAaccounts(response)
				setPage(1)
				setPagesData(createDataPages(response));
			} catch (err) {
				setError(err instanceof Error ? err.message : 'Unknown Error: getAccounts');
				setAccounts([]);
				setSearcAaccounts(null);
				setPage(1);
				setPagesData([]);
			} finally {
				setLoading(false);
				setInputValue('')
			}
		};
		fetchData();
	}, []);

	// Функція пошуку.
	function handleSearch(value:string) {
		setInputValue(value)
		const result = accounts.filter((e) => {
			if (e.email.toLowerCase().includes(value.toLowerCase())) {
				return e.email.toLowerCase().includes(value.toLowerCase());
			}
			else if (e.creationDate.includes(value)) {
				return e.creationDate.includes(value);
			}else return e.accountId.includes(value);
		});
		setSearcAaccounts(result);
		setPage(1);
		setPagesData(createDataPages(result));
	}

	// Пагінація сторінок.
	const handleNextPageClick = useCallback(() => {
		const current = page;
		const next = current + 1;
		const total = pagesData ? pagesData.length : current;

		setPage(next <= total ? next : current);
	}, [page, pagesData]);

	const handlePrevPageClick = useCallback(() => {
		const current = page;
		const prev = current - 1;

		setPage(prev > 0 ? prev : current);
	}, [page]);

	// Сортування таблиці.
	function sortAccountsEmailZa() {
		if (!searcAaccounts){return}
		const res = [...searcAaccounts].sort(function (a, b) {
			if (a.email > b.email) {
				return -1;
			}
			if (a.email < b.email) {
				return 1;
			}
			return 0;
		});
		setPagesData(createDataPages([...res]));
	}

	function sortAccountsEmailAz() {
		if (!searcAaccounts) {
			return;
		}
		const res = [...searcAaccounts].sort(function (a, b) {
			if (a.email > b.email) {
				return 1;
			}
			if (a.email < b.email) {
				return -1;
			}
			return 0;
		});
		setPagesData(createDataPages([...res]));
	}

	if (loading) return <Loading />;
	if (error) return <Message content={error} />;

	return (
		<>
			<h1>Accounts</h1>
			{pagesData ? (
				<AccountTable
					data={pagesData[page - 1]}
					inputValue={inputValue}
					location={location}
					onSortAccountsEmailAz={sortAccountsEmailAz}
					onSortAccountsEmailZa={sortAccountsEmailZa}
					onChangeInput={handleSearch}
				/>
			) : (
				<Message content={'Unfortunately, there are no data'} />
			)}

			{pagesData.length > 1 ? (
				<Pagination
					onNextPageClick={handleNextPageClick}
					onPrevPageClick={handlePrevPageClick}
					disable={{
						left: page === 1,
						right: page === pagesData.length
					}}
					nav={{ current: page, total: pagesData.length }}
				/>
			) : null}
		</>
	);
}
