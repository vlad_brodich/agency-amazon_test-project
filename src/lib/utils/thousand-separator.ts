export function thousandSeparator(num: number): string {
	const reg = /\d{1,3}(?=(\d{3})+(?!\d))/g;
	return num.toString().replace(reg, '$& ').replace(/\./g, ',');
}
