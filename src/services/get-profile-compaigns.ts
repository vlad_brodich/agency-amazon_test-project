import data from '../fake-data/fake-data.json'
import { Account } from '../types/account.type';


export function getProfileCompaigns(id: string) {
  let res
  const dataAccounts = data as Account[]

  dataAccounts.forEach(e => {
			e.profiles.forEach(el => {
				if (el.profileId === id) {
					res = [...el.campaigns];
				}
			});
  });
  return res?res:[]
}
