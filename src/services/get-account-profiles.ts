import data from '../fake-data/fake-data.json'
import { Account } from '../types/account.type';

export function getAccountProfiles(id: string) {
	const dataAccounts = data as Account[];
	const res = dataAccounts.find(e => e.accountId === id);
	return res ? res.profiles : [];
}