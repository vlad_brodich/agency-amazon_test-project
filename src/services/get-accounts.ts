import data from '../fake-data/fake-data.json';
import { Account } from '../types/account.type';

export function createDataPages(dataAccounts:Account[]) {
  const size = 5
  const datapages = []
  for (let i = 0; i <Math.ceil(dataAccounts.length/size); i++){
    datapages[i] = dataAccounts.slice((i*size), (i*size) + size);
  }
  return datapages
}


export function getAccounts() {
  const dataAccounts = data
	return dataAccounts?dataAccounts:[]
}